package net.cubekrowd.autorespawn;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

public class Main extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    // Run in default priority. Other plugins can modify the respawn location by
    // running in a higher priority.
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        var p = e.getPlayer();
        e.setRespawnLocation(p.getWorld().getSpawnLocation().add(0.5, 0, 0.5));
    }

    @EventHandler
    public void onPlayerSpawn(PlayerSpawnLocationEvent e) {
        var p = e.getPlayer();
        e.setSpawnLocation(p.getWorld().getSpawnLocation().add(0.5, 0, 0.5));
    }
}
